import unittest
import main

class MeowTest(unittest.TestCase):
    def setUp(self):
        self.client = main.app.test_client()


    def test_index(self):
        ret = self.client.get("/")
        self.assertIn(b"Meow", ret.data)

    def test_kittycounter(self):
        ret = self.client.get("/42")
        self.assertEquals(84, int(ret.data))
