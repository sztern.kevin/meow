FROM python:3.6-slim

WORKDIR /usr/src/app

COPY Pipfile ./
COPY Pipfile.lock ./

RUN pip install pipenv && pipenv install --system

COPY . .

ENV FLASK_APP main
CMD [ "gunicorn", "-b", "0.0.0.0", "main:app" ]
