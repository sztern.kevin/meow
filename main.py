from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    return "Meow-lo world!"

@app.route("/<int:n>")
def kittycounter(n):
    return str(n * 2)
